import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class AllDocuments {
	//Map that contains document name and List of n word sequencess in given text
	private Map<String, List<String>> allDocs = new TreeMap<String, List<String>>();
			
			
	//Constructor		
	public AllDocuments(String directoryPath,int sequenceSize) throws FileNotFoundException{
		allDocs = createCollection(directoryPath,sequenceSize);
	}
		
	public Map<String, List<String>> getAllDocs(){
		return this.allDocs;
	}
	
	//Creates map collection
	public static Map<String, List<String>> createCollection(String directoryPath,int sequenceSize) throws FileNotFoundException{
		File folder = new File(directoryPath);
		File[] listOfFiles = folder.listFiles();
		Map<String, List<String>> allDocs = new TreeMap<String, List<String>>();
		
		for (File file : listOfFiles) {
			//Checks if current file is a text file
		    if (file.isFile() && file.getName().endsWith(".txt")) {
		    	String path = file.getPath();
		    	allDocs.put(file.getName(), Document.createSequences(path, sequenceSize));
		    }
		}			
		return allDocs;
	}
}