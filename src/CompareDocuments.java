import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;


public class CompareDocuments {
	private static  Map<String,Integer> comparisonResults = new TreeMap<String,Integer>();

	//method to call in main for comparing
	public static void compareAll(Map<String, List<String>> allDocs){
		Set<String> tempSet =  allDocs.keySet();
		String[] keys = tempSet.toArray(new String[tempSet.size()]);
		compareAll(keys, 1,0 ,allDocs);
		showResults(comparisonResults);
	}
	
	//returns result map
	public static Map<String,Integer> getResults(){
		return comparisonResults;
	}
	
	
	//Prints out the result
	public static void showResults(Map<String,Integer> results){
		TreeSet<Integer> similarSeq = new TreeSet<Integer>(CompareDocuments.getResults().values());
		NavigableSet<Integer> sortedResult = similarSeq.descendingSet();

		
		
		for (Integer integer : sortedResult) {
			Set<String> value = getKeysByValue(CompareDocuments.getResults(), integer);
			for (String string : value) {
				System.out.println(integer +": "+ string);
			}
		}
	}
	
	
	// Method taken from http://stackoverflow.com/questions/1383797/java-hashmap-how-to-get-key-from-value
	public static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
	    return map.entrySet()
	              .stream()
	              .filter(entry -> entry.getValue().equals(value))
	              .map(entry -> entry.getKey())
	              .collect(Collectors.toSet());
	}

	
	//Help method
	public static void compareAll(String[] array, int index,int start,Map<String, List<String>> allDocs){
		
		for (; index <= array.length; index++) {
			if (index == array.length && start< array.length-1  ) {
				compareAll(array, start+2,start+1, allDocs);				
			} else {
				if (start< array.length-1 ) {
					int similarityCount = compareDocs(allDocs.get(array[start]), allDocs.get(array[index]));
					String comparableDocNames = array[start].replaceAll(".txt", "")+" - "+ array[index].replaceAll(".txt", "");
					if (similarityCount > 0) CompareDocuments.getResults().put(comparableDocNames,similarityCount);
				}
			}
		}
	}
	
	//Compares 2 given documents and returns similarity count
	public static int compareDocs(List<String> list1,List<String> list2){
		int similarCount = 0;
		for (String string1 : list1) {
			for (String string2 : list2) {
				if (string1.equalsIgnoreCase(string2)) {
					similarCount++;
					break;
				}
			}
		}
		return similarCount;
	}
	
}
