import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;


public class Main {
	
	public static void main(String[] args) throws IOException {
		
		//Timer start
		long startTime = System.currentTimeMillis();

		//Main commands
		AllDocuments allDocs = new AllDocuments("MediumDocSet",6);
		CompareDocuments.compareAll(allDocs.getAllDocs());
		
		//End timer
		long endTime = System.currentTimeMillis();
		System.out.println("That took " + (endTime - startTime) + " milliseconds");
	}
	

	
	//File Proccessing Demo method
	public static void fileProcDemo(String directoryPath,int sequenceSize) throws FileNotFoundException{
		File folder = new File(directoryPath);
		File[] listOfFiles = folder.listFiles();
		
		
		for (File file : listOfFiles) {
		    if (file.isFile() && file.getName().endsWith(".txt")) {
		    	String path = file.getPath();
		        Document studentsWork = new Document(path,sequenceSize);
		        List<String> studentWorkPieces = studentsWork.getTextPieces();
		        
	
		        for (String string : studentWorkPieces) {
					System.out.println(string);
				}
		        
		    }
		}
		
		
	}
	
}
