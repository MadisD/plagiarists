import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Document {
	private List<String> sequenceList = new ArrayList<String>();
	
	//Constructor
	public Document(String filePath,int seqSize) throws FileNotFoundException{
		sequenceList = createSequences(filePath, seqSize);
	}
	
	public List<String> getTextPieces(){
		return this.sequenceList;
	}
	
	//Method to create n sequences and return it as a list of those sequences
	public static List<String> createSequences(String filePath,int seqSize) throws FileNotFoundException{
		
		Scanner inputFile = new Scanner(new File(filePath));
		List<String> textPieces = new ArrayList<String>();
		
		while (inputFile.hasNextLine()){
			String line = inputFile.nextLine().trim();
			if (line.trim().isEmpty() == false) {
				String[] tempSplit = line.split(" ");
				
				String stringToReturn = "";
				int sequenceCounter = 0;
				
				for (int i = 0;i<tempSplit.length;i++) {
					String tempString = tempSplit[i].replaceAll("[,.]$", "").toLowerCase().replaceAll(" ", "").trim();
					
					if (sequenceCounter<seqSize) {
						stringToReturn += tempString+" ";
						sequenceCounter++;
						if (sequenceCounter == seqSize) {
							textPieces.add(stringToReturn);
							sequenceCounter = 0;
							stringToReturn = "";
						}
					} else {
						stringToReturn += tempString+" ";
					}
				}
			}
		}
		inputFile.close();
		return textPieces;
	}

}
